let _dragPos = null;
let _dragScrollPos = null;
let _dragging = false;
let _scrollPos = new Position2(0, 0);
let _mousePos = new Position2(0, 0);
let tileSize = 4096;
let origSize = 4096
let scale = 1;

var canvas = document.querySelector('canvas');
let ctx = canvas.getContext('2d');
let body = document.querySelector('body');
var _tmpCanvas = document.createElement("canvas");
var _tmpCtx = _tmpCanvas.getContext("2d");
var viewport = new Rect();
var elTiles:HTMLElement = document.querySelector('#tiles');
var _elOverlay:HTMLElement = document.querySelector('#overlay');
//_elOverlay.onmousemove = onMouseMove;
//_elOverlay.onmouseup = onMouseUp;

document.onmouseup = onMouseUp;
document.onmousemove = onMouseMove;
document.onmousedown = onMouseDown;

window.onresize = function(){
	canvas.width = body.clientWidth;
	canvas.height = body.clientHeight;
	_tmpCanvas.width = body.clientWidth;
	_tmpCanvas.height = body.clientHeight;	
	renderCanvas();
}

function onMouseDown(event:MouseEvent) {
	if (!_dragging) {
		_dragPos = new Position2(event.clientX, event.clientY);
	}
}

function onMouseUp(event:MouseEvent) {
	if (_dragging) {
		_dragging = false;
		//_elOverlay.style.display = "none";
		event.preventDefault();
		event.stopPropagation();
	}
}

function onMouseMove(event:MouseEvent) {
	var pos = new Position2(event.clientX, event.clientY);

	if (event.buttons == 1) {
		if (event.movementX || event.movementY) {
			if (!_dragging) {
				var dist = pos.dist(_dragPos);
				if (dist > 3) {
					_dragging = true;
					_dragScrollPos = new Position2(_scrollPos.x, _scrollPos.y);
					//_elOverlay.style.display = "block";
				}
			}
			else if (_dragging) {
				var diff = pos.diff(_dragPos);
				diff.x += _dragScrollPos.x;
				diff.y += _dragScrollPos.y;
				setScrollPos(diff);
			}
		}
	}
}

document.onmousewheel = function (event:MouseWheelEvent) {
	_mousePos = new Position2(event.clientX, event.clientY);

	var speed = tileSize * .2;
	var delta = -event.deltaY;
	delta = delta > 0 ? speed : -speed;

	var newSize = tileSize + delta;
	if (newSize < 20) {
		newSize = 20;
	}

	var origWorldPos = new Position2(_mousePos.x, _mousePos.y);
	origWorldPos.x -= _scrollPos.x;
	origWorldPos.y -= _scrollPos.y;

	var percentChange = newSize / tileSize;
	var newWorldPos = new Position2(origWorldPos.x * percentChange, origWorldPos.y * percentChange);
	var newScrollPos = new Position2(-newWorldPos.x + _mousePos.x, -newWorldPos.y + _mousePos.y);
	setTileSize(newSize);
	setScrollPos(newScrollPos);

	event.preventDefault();
}

function setScrollPos(pos) {
	_scrollPos.x = pos.x;
	_scrollPos.y = pos.y;

	elTiles.style.left = pos.x;
	elTiles.style.top = pos.y;

	renderCanvas();
}

function setTileSize(size) {
	tileSize = size;
	scale = tileSize / origSize;

	elTiles.style.width = xTiles * tileSize + 'px';
	elTiles.style.height = yTiles * tileSize + 'px';
}

var tiles:Tile[] = [];
let xTiles = 10;
let yTiles = 10;

var totalW = xTiles * origSize;
var totalH = yTiles * origSize;

setTileSize(origSize);

window.onresize(null);
for (let y = 0; y < yTiles; y++) {
	for (let x = 0; x < xTiles; x++) {
		tiles.push(new Tile(x, y));
	}
}