interface IImageData {
	width: number;
	height: number;
}

interface IFile {
	name: string;
	type: string;
	src: string;
}

var pica: any;
var pica2: any = pica({ features: ['js','ww'] });

class Position2 {
	x: number;
	y: number;

	constructor(x = 0, y = 0) {
		this.x = x;
		this.y = y;
	}

	diff(other) {
		return new Position2(this.x - other.x, this.y - other.y);
	}

	length() {
		return Math.abs(this.x) + Math.abs(this.y);
	}

	dist(other) {
		var diff = this.diff(other);
		return diff.length();
	}
}

function getPercent(fraction): string {
	return Math.round(fraction * 100) + '%';
}

var promise:Promise<any> = null;

let canvas2 = document.createElement("canvas");
canvas2.width = 256;
canvas2.height = 256;

async function processImage(image:HTMLImageElement){
	if(promise){
		await promise;
	}
	
	promise = pica2.resize(image, canvas2, {})
		.then(result => {
			console.log('resize done!');
		});
}

class Tile {
	x: number;
	y: number;
	imageData: HTMLImageElement | HTMLCanvasElement | ImageBitmap;
	image: HTMLImageElement;

	constructor(x, y) {
		this.x = x;
		this.y = y
		this.imageData = null;
		this.image = new Image();
		let idx = Math.floor(Math.random() * 20) + 1;     // returns a random integer from 1 to 20
		this.image.src = `images/largeImage${idx}.png`;
		this.image.onload = () => {
			/*
			let image = this.image;
			image.style.width = getPercent(image.width / totalW);
			image.style.height = getPercent(image.height / totalH);
			image.style.left = getPercent(this.x * image.width / totalW);
			image.style.top = getPercent(this.y * image.width / totalH);

			this.imageData = image;
			*/

			console.log("loaded Image");
			processImage(this.image);

			


			/*let mipmapScale = 8;
			let canvas = document.createElement("canvas");
			canvas.width = this.image.width/mipmapScale;
			canvas.height = this.image.height/mipmapScale;
			let ctx = canvas.getContext("2d");
			ctx.drawImage(this.image, 0, 0, canvas.width, canvas.height);*/
			//this.imageData = canvas;*/

			//this.imageData = this.image;
			//renderCanvas();


			/*let tmpImage = new Image();
			tmpImage.src = canvas.toDataURL();
			tmpImage.onload = () => {
				this.imageData = tmpImage;
				renderCanvas();
			}*/

			/*
			createImageBitmap(this.image).then(r => {
				this.imageData = r;
				renderCanvas();
			});
			*/
		}
	}
}