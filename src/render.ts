class Rect {
	left: number
	top: number
	right: number
	bottom: number

	constructor(left = 0, top = 0, width = 0, height = 0) {
		this.left = left;
		this.top = top;
		this.right = left + width;
		this.bottom = top + height;
	}

	get width() {
		return this.right - this.left;
	}

	get height() {
		return this.bottom - this.top;
	}

	get empty() {
		return this.width <= 0 || this.height <= 0;
	}

	scale(factor: number) {
		this.top *= factor;
		this.left *= factor;
		this.bottom *= factor;
		this.right *= factor;
	}

	revScale(factor: number) {
		this.top /= factor;
		this.left /= factor;
		this.bottom /= factor;
		this.right /= factor;
	}

	intersects(other: Rect): boolean {
		return !(this.right < other.left || this.left > other.right || this.bottom < other.top || this.top > other.bottom);
	}

	adjustToIntersect(other: Rect) {
		if (!this.intersects(other)) {
			this.left = this.right = this.top = this.bottom = 0;
			return;
		}

		if (this.left < other.left && other.left <= this.right) {
			this.left = other.left;
		}

		if (this.right > other.right && other.right >= this.left) {
			this.right = other.right;
		}

		if (this.top < other.top && other.top <= this.bottom) {
			this.top = other.top;
		}

		if (this.bottom > other.bottom && other.bottom >= this.top) {
			this.bottom = other.bottom;
		}
	}
}

var renderOffscreen = true;
var draws = 0;

function renderCanvas() {
	draws = 0;
	updateViewport();
	renderImageTags();
	/*renderOffscreenCanvas2(renderOffscreen ? _tmpCtx : ctx);
	if (renderOffscreen) {
		ctx.drawImage(_tmpCanvas, 0, 0);
	}*/
	console.log('draws: ' + draws);
}

function updateViewport() {
	viewport.left = -_scrollPos.x;
	viewport.top = -_scrollPos.y;
	viewport.right = viewport.left + canvas.width;
	viewport.bottom = viewport.top + canvas.height;
}

function renderOffscreenCanvas2(ctx: CanvasRenderingContext2D) {
	ctx.fillStyle = 'yellow';
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	for (let i = 0; i < tiles.length; i++) {
		let t = tiles[i];
		if (t.imageData) {
			let rect = new Rect(t.x * tileSize, t.y * tileSize, tileSize, tileSize);
			rect.adjustToIntersect(viewport);
			if (!rect.empty) {
				let xPos = _scrollPos.x + (t.x * tileSize);
				let yPos = _scrollPos.y + (t.y * tileSize);
				ctx.drawImage(t.imageData, xPos, yPos, tileSize, tileSize);
				draws++;
			}
		}
	}
}

function renderOffscreenCanvas(ctx: CanvasRenderingContext2D) {
	ctx.fillStyle = 'yellow';
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	for (let i = 0; i < tiles.length; i++) {
		let t = tiles[i];
		if (t.imageData) {

			let xPos = t.x * tileSize;
			let yPos = t.y * tileSize;
			let imgW = t.imageData.width * scale;
			let imgH = t.imageData.height * scale;
			let rect = new Rect(xPos, yPos, imgW, imgH);

			rect.adjustToIntersect(viewport);

			if (!rect.empty) {
				let xP = rect.left - viewport.left;
				let yP = rect.top - viewport.top;

				rect.left -= xPos;
				rect.right -= xPos;
				rect.top -= yPos;
				rect.bottom -= yPos;

				let xPercent = rect.width / imgW;
				let yPercent = rect.height / imgH;

				rect.revScale(scale);
				ctx.drawImage(t.imageData, rect.left, rect.top, rect.width, rect.height, xP, yP, tileSize * xPercent, tileSize * yPercent);
			}
		}
	}
}

function renderImageTags() {
	for (let i = 0; i < tiles.length; i++) {
		let t = tiles[i];
		if (t.imageData) {
			let rect = new Rect(t.x * tileSize, t.y * tileSize, tileSize, tileSize);
			if (rect.intersects(viewport)) {
				if(t.image.parentElement == null){
					elTiles.appendChild(t.image);
				}
				draws++;
			}else{
				t.image.remove();
			}
		}
	}
}